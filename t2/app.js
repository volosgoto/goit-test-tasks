// Get DOM Elements
const render = document.querySelector('[data-action="create"]');
const destroy = document.querySelector('[data-action="destroy"]');
const boxes = document.getElementById('boxes');
const controls = document.querySelector('#controls input');
//

// Get Amount
const getAmount = () => {
    let amount = controls.value;
    createBoxes(amount);
};
render.addEventListener('click', getAmount);
//

// Create boxes
const createBoxes = (amount) => {
    const basicSize = 30;
    let fragment = document.createDocumentFragment();

    for (let i = 0; i < amount; i++) {
        let size = basicSize + i * 10;
        let div = document.createElement('div');
        div.style.cssText = `width: ${size}px; height: ${size}px; background-color: rgba( ${random()} , ${random()} , ${random()} )`;
        fragment.appendChild(div);
    }

    boxes.appendChild(fragment);
};

// Clear Boxes
const destroyBoxes = () => {
    boxes.innerHTML = '';
};

destroy.addEventListener('click', destroyBoxes);
//

// Random color for boxes
const random = () => {
    return Math.floor(Math.random() * 256);
};
